package com.teste.projetospring.controllers.form;

import com.teste.projetospring.orm.Cargo;
import com.teste.projetospring.orm.Funcionario;
import com.teste.projetospring.repository.CargoRepository;
import com.teste.projetospring.repository.UnidadeTrabalhoRepository;

import java.time.LocalDate;
import java.util.List;


public class FuncionarioForm {
    private String nome;
    private String cpf;
    private Double salario;
    private LocalDate dataContratacao = LocalDate.now();
    private String cargoDesc;



        public Funcionario converter(CargoRepository cargoRepository, UnidadeTrabalhoRepository unidadeTrabalhoRepository) {
            List<Cargo> cargos = cargoRepository.findByDescricao(cargoDesc);
            Cargo cargo = new Cargo();
            for (Cargo car : cargos){
                cargo.setId(car.getId());
                cargo.setDescricao(car.getDescricao());
            }

            return new Funcionario(nome,cpf,salario,dataContratacao, cargo);
    }

        public String getNome () {
            return nome;
        }

        public void setNome (String Nome){
            nome = Nome;
        }

        public String getCpf () {
            return cpf;
        }

        public void setCpf (String cpf){
            this.cpf = cpf;
        }

        public Double getSalario () {
            return salario;
        }

        public void setSalario (Double salario){
            this.salario = salario;
        }

        public LocalDate getDataContratacao () {
            return dataContratacao;
        }

        public void setDataContratacao (LocalDate dataContratacao){
            this.dataContratacao = dataContratacao;
        }

        public String getCargoDesc() {
        return cargoDesc;
    }

        public void setCargoDesc(String cargoDesc) {
        this.cargoDesc = cargoDesc;
    }

}



