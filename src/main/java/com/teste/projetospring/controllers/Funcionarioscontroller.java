package com.teste.projetospring.controllers;

import com.teste.projetospring.controllers.dto.FuncionariosDTO;
import com.teste.projetospring.controllers.form.FuncionarioForm;
import com.teste.projetospring.orm.Funcionario;
import com.teste.projetospring.repository.CargoRepository;
import com.teste.projetospring.repository.FuncionarioRepository;
import com.teste.projetospring.repository.UnidadeTrabalhoRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/funcionarios")
public class Funcionarioscontroller {

    private final CargoRepository cargoRepository;
    private final FuncionarioRepository funcionarioRepository;
    private final UnidadeTrabalhoRepository unidadeTrabalhoRepository;

    public Funcionarioscontroller(FuncionarioRepository funcionarioRepository, CargoRepository cargoRepository, UnidadeTrabalhoRepository unidadeTrabalhoRepository){
        this.cargoRepository = cargoRepository;
        this.funcionarioRepository = funcionarioRepository;
        this.unidadeTrabalhoRepository = unidadeTrabalhoRepository;
    }

    @GetMapping
    public List<FuncionariosDTO> visualizar( String nome,Double salario){
        List<Funcionario> funcionarios = null;
        funcionarios = funcionarioRepository.findByNomeAndSalarioGreaterThan(nome,salario);
        return FuncionariosDTO.converter(funcionarios);
    }



    @PostMapping
    public ResponseEntity<FuncionariosDTO> cadastrar(@RequestBody FuncionarioForm funcionarioForm, UriComponentsBuilder uriBuilder){
        Funcionario funcionario = funcionarioForm.converter(cargoRepository, unidadeTrabalhoRepository);
        funcionarioRepository.save(funcionario);

        URI uri =uriBuilder.path("/funcionarios/{id}").buildAndExpand(funcionario.getId()).toUri();
        return ResponseEntity.created(uri).body(new FuncionariosDTO(funcionario));

    }




}


