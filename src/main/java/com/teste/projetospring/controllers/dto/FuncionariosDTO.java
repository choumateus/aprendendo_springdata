package com.teste.projetospring.controllers.dto;

import com.teste.projetospring.orm.Cargo;
import com.teste.projetospring.orm.Funcionario;


import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class FuncionariosDTO {
    private Integer id;
    private String nome;
    private String cpf;
    private Double salario;
    private LocalDate dataContratacao;
    private Cargo cargo;



    public FuncionariosDTO(Funcionario funcionario){
        this.id = funcionario.getId();
        this.nome = funcionario.getNome();
        this.cpf = funcionario.getCpf();
        this.salario = funcionario.getSalario();
        this.dataContratacao = funcionario.getDataContratacao();
        this.cargo = funcionario.getCargo();
    }

    public Integer getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getCpf() {
        return cpf;
    }

    public Double getSalario() {
        return salario;
    }

    public LocalDate getDataContratacao() {
        return dataContratacao;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public static List<FuncionariosDTO> converter(List<Funcionario> funcionarios) {
        return funcionarios.stream().map(FuncionariosDTO::new).collect(Collectors.toList());
    }

}
