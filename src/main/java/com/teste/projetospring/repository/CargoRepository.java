package com.teste.projetospring.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.teste.projetospring.orm.Cargo;

import java.util.List;


@Repository
public interface CargoRepository extends CrudRepository<Cargo,Integer> {
    List<Cargo> findByDescricao(String descricao);
}
