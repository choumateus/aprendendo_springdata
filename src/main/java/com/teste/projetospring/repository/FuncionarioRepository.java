package com.teste.projetospring.repository;

import com.teste.projetospring.orm.Funcionario;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FuncionarioRepository extends CrudRepository<Funcionario,Integer> {
    List<Funcionario> findByNome(String nome);
    List<Funcionario> findBySalarioGreaterThan(Double salario);
    List<Funcionario> findByCpf(String cpf);
    List<Funcionario> findByNomeAndSalarioGreaterThan(String nome, Double salario);
}
