package com.teste.projetospring.repository;

import com.teste.projetospring.orm.UnidadeTrabalho;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UnidadeTrabalhoRepository extends CrudRepository<UnidadeTrabalho,Integer>{
    List<UnidadeTrabalho> findByDescricao(String descricao);
}
