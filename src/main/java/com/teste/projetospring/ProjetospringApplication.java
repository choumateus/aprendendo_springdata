package com.teste.projetospring;

import com.teste.projetospring.orm.Cargo;
import com.teste.projetospring.repository.CargoRepository;
import com.teste.projetospring.service.CrudCargoService;
import com.teste.projetospring.service.CrudFuncionarioService;
import com.teste.projetospring.service.CrudUnidadeTrabalhoService;
import com.teste.projetospring.service.RelatoriosService;
import org.hibernate.cfg.annotations.reflection.XMLContext;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;

@SpringBootApplication
public class ProjetospringApplication implements CommandLineRunner {

	private final CrudCargoService cargoService;
	private final CrudFuncionarioService funcionarioService;
	private final CrudUnidadeTrabalhoService unidadeTrabalhoService;
	private final RelatoriosService relatoriosService;

	public ProjetospringApplication(RelatoriosService relatoriosService, CrudFuncionarioService funcionarioService, CrudCargoService cargoService, CrudUnidadeTrabalhoService unidadeTrabalhoService) {
		this.unidadeTrabalhoService = unidadeTrabalhoService;
		this.funcionarioService = funcionarioService;
		this.cargoService = cargoService;
		this.relatoriosService = relatoriosService;
	}


	public static void main(String[] args) {
		SpringApplication.run(ProjetospringApplication.class, args);
	}

	private Integer funcao(Scanner sc) {
		System.out.println("Qual area deseja mexer nos dados?");
		System.out.println("0 - sair");
		System.out.println("1 - Funcionario");
		System.out.println("2 - Cargo");
		System.out.println("3 - Unidade");
		System.out.println("4 - Relatorios");
		int area = sc.nextInt();
		return area;
	}

	@Override
	public void run(String... args) throws Exception {
		Scanner sc = new Scanner(System.in);
		boolean system = true;
		while (system) {
			int area = funcao(sc);
			switch (area) {
				case 1 : funcionarioService.inicial(sc);
					break;
				case 2 : cargoService.inicial(sc);
					break;
				case 3 : unidadeTrabalhoService.inicial(sc);
					break;
				case 4 : relatoriosService.inicial(sc);
					break;
				default: system = false;
			}
		}
	}
}
