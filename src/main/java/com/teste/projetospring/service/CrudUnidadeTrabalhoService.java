package com.teste.projetospring.service;

import com.teste.projetospring.orm.Cargo;
import com.teste.projetospring.orm.UnidadeTrabalho;
import com.teste.projetospring.repository.FuncionarioRepository;
import com.teste.projetospring.repository.UnidadeTrabalhoRepository;
import org.springframework.stereotype.Service;

import java.util.Scanner;

@Service
public class CrudUnidadeTrabalhoService {
    private boolean system = true;
    private final UnidadeTrabalhoRepository unidadeTrabalhoRepository;

    public CrudUnidadeTrabalhoService(UnidadeTrabalhoRepository  unidadeTrabalhoRepository){
        this.unidadeTrabalhoRepository = unidadeTrabalhoRepository;
    }
    public void inicial(Scanner sc){
        while (system){
            System.out.println("Qual acao de unidade deseja executar");
            System.out.println("0 - Sair");
            System.out.println("1 - Salvar");
            System.out.println("2 - Atualizar");
            System.out.println("3 - Visualizar");
            System.out.println("4 - Deletar");
            int action = sc.nextInt();
            switch(action){
                case 1 : salvar(sc);
                    break;
                case 2: atualizar(sc);
                    break;
                case 3: visualizar();
                    break;
                case 4: deletar(sc);
                    break;
                default: system = false;
                    break;
            }
        }
    }
    private void salvar(Scanner sc) {
        System.out.println("Digite o nome da unidade");
        String nome = sc.next();

        System.out.println("Digite o endereco");
        String endereco = sc.next();

        UnidadeTrabalho unidadeTrabalho = new UnidadeTrabalho();
        unidadeTrabalho.setDescricao(nome);
        unidadeTrabalho.setEndereco(endereco);
        unidadeTrabalhoRepository.save(unidadeTrabalho);
        System.out.println("Salvo");
    }
    private void atualizar(Scanner sc){
        System.out.println("Id");
        int id = sc.nextInt();
        System.out.println("nome da unidade");
        String nome = sc.next();
        System.out.println("Endereco");
        String endereco = sc.next();

        UnidadeTrabalho unidadeTrabalho = new UnidadeTrabalho();
        unidadeTrabalho.setId(id);
        unidadeTrabalho.setDescricao(nome);
        unidadeTrabalho.setEndereco(endereco);
        unidadeTrabalhoRepository.save(unidadeTrabalho);
        System.out.println("Atualizado");

    }
    private void visualizar(){
        Iterable<UnidadeTrabalho> unidadeTrabalhos = unidadeTrabalhoRepository.findAll();
        unidadeTrabalhos.forEach(unidadeTrabalho -> System.out.println(unidadeTrabalho));

    }
    private void deletar(Scanner sc){
        System.out.println("Id");
        int id = sc.nextInt();
        unidadeTrabalhoRepository.deleteById(id);
        System.out.println("Deletado");

    }

}
