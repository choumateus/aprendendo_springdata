package com.teste.projetospring.service;

import com.teste.projetospring.orm.Funcionario;
import com.teste.projetospring.repository.FuncionarioRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Scanner;

@Service
public class RelatoriosService {
    private boolean system = true;

    private final FuncionarioRepository funcionarioRepository;

    public RelatoriosService(FuncionarioRepository funcionarioRepository){
        this.funcionarioRepository = funcionarioRepository;
    }

    public void inicial(Scanner sc){
        while (system){
            System.out.println("Qual acao de funcionario deseja executar");
            System.out.println("0 - Sair");
            System.out.println("1 - Busca funcionario pelo nome");
            System.out.println("2 - Busca funcionarios com salario maior");
            System.out.println("3 - Busca funcionario por cpf");
            int action = sc.nextInt();
            switch(action){
                case 1 :buscaFuncionarioNome(sc); ;
                    break;
                case 2 :buscaFuncionarioSalario(sc);
                    break;
                case 3 : buscaFuncionarioCpf(sc);
                    break;
                default: system = false;
                    break;
            }
        }
    }
    private void buscaFuncionarioSalario(Scanner sc){
        System.out.println("Qual salario minimo deseja pesquisar");
        Double salario = sc.nextDouble();
        List<Funcionario> list = funcionarioRepository.findBySalarioGreaterThan(salario);
        list.forEach(System.out::println);
    }
    private void buscaFuncionarioNome(Scanner sc){
        System.out.println("Qual nome deseja pesquisar");
        String nome = sc.next();
        List<Funcionario> list = funcionarioRepository.findByNome(nome);
        list.forEach(System.out::println);
    }
    private void buscaFuncionarioCpf(Scanner sc){
        System.out.println("Qual cpf deseja pesquisar");
        String cpf = sc.next();
        List<Funcionario> list = funcionarioRepository.findByCpf(cpf);
        list.forEach(System.out::println);
    }
}
