package com.teste.projetospring.service;

import com.teste.projetospring.orm.Cargo;
import com.teste.projetospring.orm.Funcionario;
import com.teste.projetospring.orm.UnidadeTrabalho;
import com.teste.projetospring.repository.CargoRepository;
import com.teste.projetospring.repository.FuncionarioRepository;
import com.teste.projetospring.repository.UnidadeTrabalhoRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

@Service
public class CrudFuncionarioService {
    private boolean system = true;
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    private final CargoRepository cargoRepository;
    private final FuncionarioRepository funcionarioRepository;
    private final UnidadeTrabalhoRepository unidadeTrabalhoRepository;

    public CrudFuncionarioService(FuncionarioRepository funcionarioRepository, CargoRepository cargoRepository, UnidadeTrabalhoRepository unidadeTrabalhoRepository){
        this.cargoRepository = cargoRepository;
        this.funcionarioRepository = funcionarioRepository;
        this.unidadeTrabalhoRepository = unidadeTrabalhoRepository;
    }
    public void inicial(Scanner sc){
        while (system){
            System.out.println("Qual acao de funcionario deseja executar");
            System.out.println("0 - Sair");
            System.out.println("1 - Salvar");
            System.out.println("2 - Atualizar");
            System.out.println("3 - Visualizar");
            System.out.println("4 - Deletar");
            int action = sc.nextInt();
            switch(action){
                case 1 : salvar(sc);
                    break;
                case 2: atualizar(sc);
                    break;
                case 3: visualizar();
                    break;
                case 4: deletar(sc);
                    break;
                default: system = false;
                    break;
            }
        }
    }
    private List<UnidadeTrabalho> unidade(Scanner sc) {
        boolean sys = true;
        List<UnidadeTrabalho> unidades = new ArrayList<>();
        while (sys) {
            System.out.println("digite a Id da unidade que quer adicionar e 0 para sair");
            int unidadeId = sc.nextInt();
            if (unidadeId != 0) {
                Optional<UnidadeTrabalho> unidade = unidadeTrabalhoRepository.findById(unidadeId);
                unidades.add(unidade.get());
            } else {
                sys = false;
            }
        }
        return unidades;
    }
    private void salvar(Scanner sc) {
        System.out.println("Nome do funcionario");
        String nome = sc.next();

        System.out.println("cpf do funcionario");
        String cpf = sc.next();

        System.out.println("salario do funcionario");
        Double salario = sc.nextDouble();

        System.out.println("data de contratacao");
        String dataContratacao = sc.next();

        System.out.println("cargoId do funcionario");
        Integer cargoId = sc.nextInt();

        List<UnidadeTrabalho> unidades = unidade(sc);


        Funcionario funcionario = new Funcionario();
        funcionario.setNome(nome);
        funcionario.setCpf(cpf);
        funcionario.setSalario(salario);
        funcionario.setDataContratacao(LocalDate.parse(dataContratacao, formatter));
        Optional<Cargo> cargo = cargoRepository.findById(cargoId);
        funcionario.setCargo(cargo.get());
        funcionario.setUnidadeTrabalhos(unidades);

        funcionarioRepository.save(funcionario);
        System.out.println("Salvo");
    }

    private void atualizar(Scanner sc){
        System.out.println("Digite o id");
        Integer id = sc.nextInt();

        System.out.println("Nome do funcionario");
        String nome = sc.next();

        System.out.println("cpf do funcionario");
        String cpf = sc.next();

        System.out.println("salario do funcionario");
        Double salario = sc.nextDouble();

        System.out.println("data de contratacao");
        String dataContratacao = sc.next();

        System.out.println("cargoId do funcionario");
        Integer cargoId = sc.nextInt();

        List<UnidadeTrabalho> unidades = unidade(sc);

        Funcionario funcionario = new Funcionario();
        funcionario.setNome(nome);
        funcionario.setCpf(cpf);
        funcionario.setSalario(salario);
        funcionario.setDataContratacao(LocalDate.parse(dataContratacao, formatter));
        Optional<Cargo> cargo = cargoRepository.findById(cargoId);
        funcionario.setCargo(cargo.get());
        funcionario.setUnidadeTrabalhos(unidades);

        funcionarioRepository.save(funcionario);
        System.out.println("Atualizado");

    }
    private void visualizar(){
        Iterable<Funcionario> funcionarios = funcionarioRepository.findAll();
        funcionarios.forEach(funcionario -> System.out.println(funcionario));
    }
    private void deletar(Scanner sc){
        System.out.println("Id");
        int id = sc.nextInt();
        funcionarioRepository.deleteById(id);
        System.out.println("Deletado");

    }

}




